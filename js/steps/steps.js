$(document).ready(function(){
  $("#modal").load("../html_inc/example-modal.html");
  $("#modal2").load("../html_inc/example-modal-2.html");
});

function claimSteps() {
  $('.claimstep').each(function(){
    stepnum = $(this).attr('stepnum');
    title = $(this).attr('title');
    iconclass = $(this).attr('iconclass');
    if(stepnum == 1){ var stepnum_text = "one"; }
    if(stepnum == 2){ var stepnum_text = "two"; }
    if(stepnum == 3){ var stepnum_text = "three"; }
    if(stepnum == 4){ var stepnum_text = "four"; }
    if(stepnum == 5){ var stepnum_text = "five"; }
    if(stepnum == 6){ var stepnum_text = "six"; }
    if(stepnum == 7){ var stepnum_text = "seven"; }
    if(stepnum == 8){ var stepnum_text = "eight"; }
    
    var titlebar = 
      '<div class="row"><div id="step' + stepnum + '" class="col-sm-12" style="display: none;">' +
        '<div class="stepbar inactive">' +
          '<i class="' + iconclass + '"></i>' +
          '<div class="stepbar-text-top">step ' + stepnum_text + '</div>' +
          '<div class="stepbar-text-bottom">' + title + '</div>' +
        '</div>' +
      '</div></div>\n';
    
    $(this).before(titlebar);
    $(this).load('../views/steps_inc/step' + stepnum + '.html');
    $(this).hide();
  });
}
