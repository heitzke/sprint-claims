//DO THESE ON WINDOW LOAD FOR PAGES WHERE TARGET ELEMENT IS PRESENT
$(window).load(function(){
	if($('.exit').length){
		$("#exitinfo").load("../views/html_inc/exitinfo.html");
	}
	if($('#locate').length){
		$("#locateinfo").load("../views/html_inc/locateinfo.html");
	}
});

// LOADING MODALS 
$(document).ready(function(){
	$("#modal").load("html_inc/example-modal.html");
	$("#modal2").load("html_inc/example-modal-2.html");
	$("#modal-ios").load("html_inc/example-modal-ios.html");
	$("#upsell-modal").load("html_inc/upsell-modal.html");
    $('.banner-alert-trigger').on('click', function(){
        $('.banner-alert').addClass('active');
    });
    $('.banner-alert-close').on('click', function(){
        $('.banner-alert').removeClass('active');
    }); 
	$('.es-link').on('click', function(){
	   $('body').addClass('lang-es');
	});
	$('.popover-trigger').popover();
	$('.progress').affix({
        offset: {
            top: 77
        }
    });

	$('.sidebar-chat').on('mouseenter', function(){
		$(this).addClass('expanded');
		$('.sidebar-chat p').addClass('animated fadeInLeft');
	});

	$('.sidebar-chat').on('mouseleave', function(){
		$(this).removeClass('expanded');
		$('.sidebar-chat p').removeClass('animated fadeInLeft');
	});

	$(window).scroll(function () {
        if ($(window).scrollTop() > 78){
        	$('.sidebar-chat').removeClass('fadeOut');
        	$('.sidebar-chat').addClass('animated fadeInDown');
        }
        else {
    	   $('.sidebar-chat').addClass('animated fadeOut');
        }
	});
});

//DO THESE ON WINDOW LOAD FOR ALL PAGES
$(window).load(function(){
	//$("#privacypolicy").load("../views/html_inc/privacypolicy.html");
	//$("#tsandcs").load("../views/html_inc/tsandcs.html");
	//$("#contactinfo").load("../views/html_inc/contactinfo.html");
	$.fn.slideFadeToggle = function(speed, easing, callback) {
    return this.animate({
      opacity: 'toggle',
      height: 'toggle'
    }, speed, easing, callback);
	};
	localStorageInit();
	localStorageGo();
});

//DO THESE ON DOCUMENT READY FOR ALL PAGES WHERE TARGET ELEMENT IS PRESENT
$(document).ready(function(){
	if($('.claimstep').length){
		claimSteps();
	}
});

/**
* Local Storage Init
*/

function localStorageInit(){
  $('input,textarea').each(function(){
  	var thisname = $(this).attr('name');
  	var thisid = $(this).attr('id');
  	if((thisname != undefined) && (thisid != undefined) && ($(this).hasClass('stored'))){
	    if (localStorage[thisname]) {
	    	$('#' + thisid).val(localStorage[thisname]);
	    }
  	}
  });
}

/**
* Local Storage Actions
*/

function localStorageGo(){
	$('.stored').keyup(function () {
	  localStorage[$(this).attr('name')] = $(this).val();
	});	
	$('#localStorageTest').submit(function() {
	  localStorage.clear();
	});
}

/**
* Checkbox Maker
*/
function cheekyBox() {
	document.createElement('cheekybox'); //crutch for IE
	$('cheekybox').each(function(){
		var cheekyid = $(this).attr('id');
		var nameid = 'name="' + cheekyid + '" id="' + cheekyid + '"';
		var divclass = $(this).attr('class');
		var size = ($(this).attr('size')=="large")? 'lg' : 'sm';
		var label = $(this).attr('label');
		var target = ($(this).attr('targetid') != undefined)? ' targetid="' + $(this).attr('targetid') + '"' : '';
		var checkboxinput = ($(this).attr('createinput')=="yes")? '<input type="hidden" ' + nameid + ' value="off" />' : '';
		
		var output = 
			'<div class="' + divclass + '">' + checkboxinput +
			'<div ' + nameid + ' class="cheekybox cheekybox-' + size + '" ' + target + ' ></div>' +
			'<label for="' + cheekyid + '" class="cheekybox-' + size + '-label">' + label + '</label>' +
			'</div>';
		$(this).replaceWith(output);
	});	
}

$('body').delegate('.cheekybox', 'click', function(){
	var cheekyid = $(this).attr('id');
	var targetid = $(this).attr('targetid');
	if($(this).hasClass('icon-checkmark')){
		$(this).removeClass('icon-checkmark');
		$('input#' + cheekyid).val("off");
	}else{
		$(this).addClass('icon-checkmark');
		$('input#' + cheekyid).val("on");
	}
});	

/**
* claimSteps
*/

function claimSteps() {
	$('.claimstep').each(function(){
		stepnum = $(this).attr('stepnum');
		title = $(this).attr('title');
		iconclass = $(this).attr('iconclass');
		if(stepnum == 1){ var stepnum_text = "one"; }
		if(stepnum == 2){ var stepnum_text = "two"; }
		if(stepnum == 3){ var stepnum_text = "three"; }
		if(stepnum == 4){ var stepnum_text = "four"; }
		if(stepnum == 5){ var stepnum_text = "five"; }
		if(stepnum == 6){ var stepnum_text = "six"; }
		if(stepnum == 7){ var stepnum_text = "seven"; }
		if(stepnum == 8){ var stepnum_text = "eight"; }
		
		var titlebar = 
			'<div class="row"><div id="step' + stepnum + '" class="col-sm-12" style="display: none;">' +
				'<div class="stepbar inactive">' +
					'<i class="' + iconclass + '"></i>' +
					'<div class="stepbar-text-top">step ' + stepnum_text + '</div>' +
					'<div class="stepbar-text-bottom">' + title + '</div>' +
				'</div>' +
			'</div></div>\n';
		
		$(this).before(titlebar);
		$(this).load('../views/steps_inc/step' + stepnum + '.html');
		$(this).hide();
	});
}

$(document).ready(function(){
	$('.claimstep[stepnum="1"]').css('display','block').addClass('animated fadeInDown');
	$('#step1').css('display', 'block').addClass('animated fadeInDown');
	$('#step1 .stepbar').removeClass('inactive');
	$('.progress i[stepnum="1"]').removeClass('inactive');

	$('.start-loader').on('click', function(){
		$('.loading').fadeIn();
		progressJs(".loading").start().autoIncrease(10, 500);
	});
	$('.stop-loader').on('click', function(){
		$('.loading').fadeOut();
		progressJs(".loading").end();
	});
});

$(window).load(function(){
	$('body').delegate('.step-continue', 'click', function(){
		cheekyBox();
		//alert('stupid cheekybox');

		var tostep = $(this).attr('tostep');
		var thisstep = $(this).parents('.claimstep').attr('stepnum');
		var nextstep = (tostep == undefined)? parseInt(thisstep,10) + 1 : tostep;

		var thisicon = $('.progress i[stepnum=' + thisstep + ']');
		var nexticon = $('.progress i[stepnum=' + nextstep + ']');

		$(thisicon).removeClass().addClass('icon-checkmark');
		$(nexticon).removeClass('inactive');

		//alert('FART DONKEY!');
		$(this).parents('.claimstep').addClass('animated fadeOut').slideUp();
		$('html, body').animate({scrollTop:85}, '1000', 'linear', function() { 
		});
		$('#step' + thisstep + ' .stepbar').fadeOut();
		$('#step' + thisstep + ' .stepbar i').removeClass().addClass('icon-checkmark');
		$('#step' + nextstep).delay(300).fadeIn();

		$('.claimstep[stepnum="' + nextstep + '"]').delay(500).fadeIn();
		$('#step' + nextstep + ' .stepbar').delay(450).removeClass('inactive');	
	});
});

/* FORM RELATED JS */
/**
* requiredInputValidation
*/

function requiredInputValidation() {
	$('.required').blur(function() {
		var inputvalue = $(this).val();
		var errmsg = $(this).attr('errmsg');
		if( inputvalue == $(this)[0].defaultValue && inputvalue.length == 0 ){
			$(this).addClass('not-verified');
			$(this).before('<span class="icon-close textbox-icon"></span>');
			$(this).after('<div class="err">' + errmsg + '</div>');
		}else{
			$(this).addClass('verified');
			$(this).before('<span class="icon-checkmark textbox-icon"></span>');
		}
	});
}

/**
* requiredMatchValidation
* adds .not-verified or .verified to input elements using .required-match
* Indicate selector for matching by setting "matchwith" attribute to the selector to match $(this).val() with. "errmsg" attribute indicates the error message that is displayed after the input when the values do not match.
*/

function requiredMatchValidation() {
	$('.required-match').focus(function() {
		$(this).removeClass('not-verified').removeClass('verified');
		$(this).prev('.textbox-icon').remove();
		$(this).next('.err').remove();
		$(this).val('');
		$('.err.submit').remove();
	});
	$('.required-match').blur(function() {
		$(this).each(function() {
			var inputvalue = $(this).val();
			var errmsg = $(this).attr('errmsg');
			var matchwith = $(this).attr('matchwith');
			var comparevalue = $(matchwith).val();
			if( inputvalue != comparevalue || ( inputvalue == $(this)[0].defaultValue && inputvalue.length == 0 )){
				$(this).addClass('not-verified');
				$(this).before('<span class="icon-close textbox-icon"></span>');
				$(this).after('<div class="err">' + errmsg + '</div>');
			}else{
				$(this).addClass('verified');
				$(this).before('<span class="icon-checkmark textbox-icon"></span>');				
			}
		});
	});
}

/**
* activateSubmit
* @param {Selector} submitselector
*/
function activateSubmit(submitselector) {
	$('.required, .required-match').focus(function() {
		$(this).removeClass('not-verified').removeClass('verified');
		$(this).prev('.textbox-icon').remove();
		$(this).next('.err').remove();
		$(this).val('');
		$('.err.submit').remove();
		if($(submitselector).hasClass('active')){
			$(submitselector).removeClass('active').addClass('disabled');
		}
	});
	$('.required,.required-match').blur(function() {
		var nreq = $('input.required, input.required-match').length;
		var nver = $('input.verified, input.required-match.verified').length;
		if (nreq == nver){
			$(submitselector).removeClass('disabled').addClass('active');
		}
	});
}
