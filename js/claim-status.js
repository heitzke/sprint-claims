/*============================
	=            TABS            =
	============================*/
	var tabMod = $('.js-tabs'); 
	var tabs = tabMod.find('[data-tab-target]'); 
	var tabAnchors = $('.js-tab-anchor');


	tabs.click(function(event) {
		event.preventDefault();
		var target = $(this).attr('data-tab-target');
		
		tabAnchors.removeClass('active').addClass('inactive');
		tabs.removeClass('tab--active');

		$('[data-tab="'+target+'"]').addClass('active').removeClass('inactive');
		$('[data-tab-target="'+target+'"]').addClass('tab--active').removeClass('inactive');
	});
	$('.tab--active').trigger('click');
	if( tabMod.hasClass('tabs--mobile-disable')){
		tabAnchors.addClass('tab-anchor--mobile-disable')
	}