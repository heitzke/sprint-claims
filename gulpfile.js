
var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    clean = require('gulp-clean'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    livereload = require('gulp-livereload'),
    watch = require('gulp-watch'),
    lr = require('tiny-lr'),
    server = lr();

gulp.task('images', function() {
  return gulp.src('./images/*')
    .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
    .pipe(gulp.dest('./images/'))
    .pipe(livereload(server))
    .pipe(notify({ message: 'Images task complete' }));
});

gulp.task('main-styles', function() {
  return gulp.src('./css/scss/**/*.scss')
    .pipe(sass({ style: 'expanded' }))
    .pipe(gulp.dest('./css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('./css'))
    .pipe(livereload(server))
    .pipe(notify({ message: 'Lets do this shit' }));
});

gulp.task('lib-styles', function() {
  return gulp.src('./css/scss/lib/*.scss')
    .pipe(sass({ style: 'expanded' }))
    .pipe(gulp.dest('./css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('./css'))
    .pipe(livereload(server))
    .pipe(notify({ message: 'Lets do this shit' }));
});

gulp.task('watch', function() {

  server.listen(35729, function (err) {
    if (err) {
      return console.log(err)
    };
      gulp.watch('./css/scss/**/*.scss', ['main-styles']);
      gulp.watch('./css/scss/lib/*.scss', ['lib-styles']);
      gulp.watch('./images/*', ['images']);
    });
});

gulp.task('default', function() {
  gulp.start('main-styles', 'images');
});
